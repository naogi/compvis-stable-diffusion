import subprocess as sp
import io
import os
import random
from uuid import uuid4

from naogi import NaogiModel, FileRenderer
from sd import Stablediffusion

class TargzRenderer(FileRenderer):
    @classmethod
    def render(cls, result):
        archive = f"/tmp/{result['filename']}.tar.gz"

        sp.check_call([
            'tar',
            '-czv',
            '--file', str(archive),
            '--directory', str(result['path']), '.'
        ])

        print(f"TARed: {archive} from {result['path']}")

        with open(archive, "rb") as file:
            bytes_io = io.BytesIO(file.read())
        bytes_io.seek(0)

        return super().render(bytes_io, filename=f"{result['filename']}.tar.gz", downloadable=True, content_type='application/gzip')

class PathFileRenderer(FileRenderer):
    @classmethod
    def render(cls, path):
        with open(path, "rb") as file:
            bytes_io = io.BytesIO(file.read())
        bytes_io.seek(0)

        filename = os.path.basename(path)

        return super().render(bytes_io, filename=filename, content_type='image/png', downloadable=True)


class Model(NaogiModel):
    def init_model(self):
        self.model = Stablediffusion(ckpt="model.ckpt")

    def warming_up(self):
        Stablediffusion(ckpt="model.ckpt")

    def load_model(self):
        sp.check_call([
            'wget',
            '-O', 'model.ckpt',
            'https://naogi-public.s3.eu-west-1.amazonaws.com/sd-v1-5-pruned-runway.ckpt'
        ])

    def prepare(self, params):
        self.prompt = str(params['prompt'])
        self.uuid = str(uuid4())
        self.output = '/tmp/' + self.uuid
        self.seed = int(params['seed']) if ('seed' in params) else int(random.random() * 1000)
        self.steps = int(params['steps']) if ('steps' in params) else 50
        self.height = int(params['height']) if ('height' in params) else 512
        self.width = int(params['width']) if ('width' in params) else 512

    def predict(self):
        image_path = self.model.predict(
            prompt=self.prompt,
            outdir=self.output,
            seed=self.seed,
            steps=self.steps,
            height=self.height,
            width=self.width
        )

        return image_path

    def renderer(self):
        return PathFileRenderer

    # def predict(self):
    #     sp.check_call([
    #         "python", "scripts/txt2img.py",
    #         "--prompt", str(self.prompt),
    #         "--ckpt", "model.ckpt",
    #         "--outdir", str(self.output),
    #         "--seed", str(self.seed),
    #         "--plms"
    #     ])

    #     return dict({
    #         'path': str(self.output),
    #         'filename': f"{self.seed}_{self.uuid}",
    #     })

    # def renderer(self):
    #     return TargzRenderer
