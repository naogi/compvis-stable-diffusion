class Config():
    def __init__(self,
                  prompt:str='Space cat',
                  outdir:str="outputs/txt2img-samples",
                  steps:int=50,
                  plms:bool=True,
                  dpm:bool=False,
                  fixed_code:bool=True,
                  ddim_eta:float=0.0,
                  n_iter:int=1,
                  H:int=512,
                  W:int=512,
                  C:int=4,
                  f:int=8,
                  n_samples:int=1,
                  n_rows:int=0,
                  scale:float=7.5,
                  from_file:str=None,
                  config:str="configs/stable-diffusion/v1-inference.yaml",
                  ckpt:str=None,
                  seed:int=42,
                  precision:str='full',
                  repeat:int=1,
                ):
        self.prompt = prompt # the prompt to render
        self.outdir = outdir # dir to write results to
        self.steps = steps # number of ddim sampling steps
        self.plms = plms # use plms sampling
        self.dpm = dpm # use DPM (2) sampler
        self.fixed_code = fixed_code # if enabled, uses the same starting code across all samples
        self.ddim_eta = ddim_eta # ddim eta (eta=0.0 corresponds to deterministic sampling
        self.n_iter = n_iter # sample this often
        self.H = H # image height, in pixel space
        self.W = W # image width, in pixel space
        self.C = C # latent channels
        self.f = f # downsampling factor, most often 8 or 16
        self.n_samples = n_samples # how many samples to produce for each given prompt. A.k.a batch size
        self.n_rows = n_rows # rows in the grid (default: n_samples)
        self.scale = scale # unconditional guidance scale: eps = eps(x, empty) + scale * (eps(x, cond) - eps(x, empty))
        self.from_file = from_file # if specified, load prompts from this file, separated by newlines
        self.config = config # path to config which constructs model
        self.ckpt = ckpt # path to checkpoint of model
        self.seed = seed # the seed (for reproducible sampling)
        self.precision = precision # evaluate at this precision choices=["full", "autocast"]
        self.repeat = repeat # repeat each prompt in file this often
        self.skip_save = False
        self.skip_grid = True
        self.ddim_steps = steps

    def call(self):
        return self
