import argparse, os, sys, glob
import torch
import numpy as np
from omegaconf import OmegaConf
from PIL import Image
# from tqdm import tqdm, trange
from itertools import islice
from einops import rearrange
from pytorch_lightning import seed_everything
from torch import autocast
from contextlib import nullcontext

from ldm.util import instantiate_from_config
from ldm.models.diffusion.ddim import DDIMSampler
from ldm.models.diffusion.plms import PLMSSampler

from config import Config

class Stablediffusion():
    def __init__(self, ckpt:str):
        self.opt = Config(ckpt=ckpt).call()

        self.device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

        config = OmegaConf.load(f"{self.opt.config}")
        model = self.load_model_from_config(config, f"{self.opt.ckpt}")

        self.model = model.to(self.device)

    def chunk(self, it, size):
        it = iter(it)
        return iter(lambda: tuple(islice(it, size)), ())


    def numpy_to_pil(self, images):
        """
        Convert a numpy image or a batch of images to a PIL image.
        """
        if images.ndim == 3:
            images = images[None, ...]
        images = (images * 255).round().astype("uint8")
        pil_images = [Image.fromarray(image) for image in images]

        return pil_images


    def load_model_from_config(self, config, ckpt, verbose=False):
        print(f"Loading model from {ckpt}")
        pl_sd = torch.load(ckpt, map_location=self.device)
        if "global_step" in pl_sd:
            print(f"Global Step: {pl_sd['global_step']}")
        sd = pl_sd["state_dict"]
        model = instantiate_from_config(config.model)
        m, u = model.load_state_dict(sd, strict=False)
        if len(m) > 0 and verbose:
            print("missing keys:")
            print(m)
        if len(u) > 0 and verbose:
            print("unexpected keys:")
            print(u)

        model.cuda()
        model.eval()
        return model


    def load_replacement(self, x):
        try:
            hwc = x.shape
            y = Image.open("assets/rick.jpeg").convert("RGB").resize((hwc[1], hwc[0]))
            y = (np.array(y)/255.0).astype(x.dtype)
            assert y.shape == x.shape
            return y
        except Exception:
            return x


    def save_sample(self, path, base_count, sample) -> str:
      sample = 255. * rearrange(sample.cpu().numpy(), 'c h w -> h w c')
      img = Image.fromarray(sample.astype(np.uint8))
      sample_path = os.path.join(path, f"{base_count:05}.png")

      img.save(sample_path)

      return sample_path


    def predict(self, outdir:str, seed:int, prompt:str, height:int=512, width:int=512, steps:int=50) -> str:
        self.opt.prompt = prompt
        self.opt.outdir = outdir
        self.opt.seed = seed
        self.opt.steps = steps
        self.opt.ddim_steps = steps
        self.opt.H = height
        self.opt.W = width

        seed_everything(self.opt.seed)

        model = self.model

        if self.opt.plms:
            sampler = PLMSSampler(model)
        else:
            sampler = DDIMSampler(model)

        os.makedirs(self.opt.outdir, exist_ok=True)
        outpath = self.opt.outdir

        batch_size = self.opt.n_samples

        prompt = self.opt.prompt
        assert prompt is not None
        data = [batch_size * [prompt]]

        sample_path = os.path.join(outpath, "samples")
        os.makedirs(sample_path, exist_ok=True)

        base_count = len(os.listdir(sample_path))

        start_code = None
        if self.opt.fixed_code:
            start_code = torch.randn(
              [self.opt.n_samples, self.opt.C, self.opt.H // self.opt.f, self.opt.W // self.opt.f],
              device=self.device
            )

        precision_scope = autocast if self.opt.precision=="autocast" else nullcontext

        with torch.no_grad(), precision_scope("cuda"), model.ema_scope():
            # for n in trange(self.opt.n_iter, desc="Sampling"):
                # for prompts in tqdm(data, desc="data"):
            for prompts in data:
                uc = None
                if self.opt.scale != 1.0:
                    uc = model.get_learned_conditioning(batch_size * [""])
                if isinstance(prompts, tuple):
                    prompts = list(prompts)
                c = model.get_learned_conditioning(prompts)
                shape = [self.opt.C, self.opt.H // self.opt.f, self.opt.W // self.opt.f]
                samples_ddim, _ = sampler.sample(S=self.opt.ddim_steps,
                                                conditioning=c,
                                                batch_size=self.opt.n_samples,
                                                shape=shape,
                                                verbose=False,
                                                unconditional_guidance_scale=self.opt.scale,
                                                unconditional_conditioning=uc,
                                                eta=self.opt.ddim_eta,
                                                x_T=start_code)

                x_samples_ddim = model.decode_first_stage(samples_ddim)
                x_samples_ddim = torch.clamp((x_samples_ddim + 1.0) / 2.0, min=0.0, max=1.0)
                x_samples_ddim = x_samples_ddim.cpu().permute(0, 2, 3, 1).numpy()

                x_checked_image_torch = torch.from_numpy(x_samples_ddim).permute(0, 3, 1, 2)

                for x_sample in x_checked_image_torch:
                    return self.save_sample(sample_path, base_count, x_sample)
