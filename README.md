# CompVis-stable-diffusion

### Build

```shell
docker build -t stable-diffusion .
```

### Execute

```shell
docker run --gpus all -v /home/ubuntu/output:/output -it -d stable-diffusion scripts/txt2img.py --prompt "space cat" --ckpt /app/model.ckpt --outdir /output --plms
```
